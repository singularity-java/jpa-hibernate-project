package com.bobocode.dao;

import com.bobocode.model.Photo;
import com.bobocode.model.PhotoComment;
import com.bobocode.util.ExerciseNotCompletedException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Please note that you should not use auto-commit mode for your implementation.
 */
public class PhotoDaoImpl implements PhotoDao {
    private EntityManagerFactory entityManagerFactory;

    public PhotoDaoImpl(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public void save(Photo photo) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityManager.persist(photo);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException("Transaction is rolled back", e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Photo findById(long id) {
         // todo
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        return entityManager.find(Photo.class, id);
    }

    @Override
    public List<Photo> findAll() {
        // todo
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            List<Photo> photos = entityManager.createQuery("select p from Photo p", Photo.class).getResultList();
            entityManager.getTransaction().commit();
            return photos;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException("Transaction is rolled back", e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(Photo photo) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            Photo mergedPhoto = entityManager.merge(photo);
            entityManager.remove(mergedPhoto);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException("Transaction is rolled back", e);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addComment(long photoId, String comment) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            Photo photo = entityManager.getReference(Photo.class, photoId);
            PhotoComment photoComment = new PhotoComment();
            photoComment.setCreatedOn(LocalDateTime.now());
            photoComment.setText(comment);
            photoComment.setPhoto(photo);
            photo.addComment(photoComment);
            entityManager.persist(photo);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException("Transaction is rolled back", e);
        } finally {
            entityManager.close();
        }
    }
}
